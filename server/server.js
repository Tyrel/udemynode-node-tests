const express = require('express');

var app = express();

app.get('/', (req, res) => {
  // res.send("it loaded");
  res.status(404).send({
    error: "Page not found",
    name: "Todo App 1.0"
  })
});

app.get('/users', (req, res) => {
  res.send([
    {name: 'Tyrel', age: 29},
    {name: 'Lauren', age: 32},
    {name: 'Max', age: 33}
  ])


});


app.listen(3000);

module.exports.app = app;
